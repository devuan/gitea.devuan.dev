gitea.devuan.dev
================

This project contains the setup and customization for
`gitea.devuan.dev`. The setup is password protected via a "vault"
directory that is mounted on a "temp" directory with `gocryptfs`. The
vault is used with scripts `vault/open.sh` and `vault/close.sh`. Vault
password is the same as the postgresql database password.

gitea setup
-----------

The setup is the single file `app.ini` which gets installed as
`/etc/gitea/app.ini`. See
https://docs.gitea.io/en-us/config-cheat-sheet/ for details.

gitea customisation
-------------------

The `custom` directory tree is installed as `/var/lib/gitea/custom`,
and it contains all WUI customisation. See
https://docs.gitea.io/en-us/customizing-gitea/ for details.

gitea installation
------------------

These are some specific details about Devuan's gitea installation.

Run user `git`, group `git`. The home area is for adminstration
purposes only, where in particular:

 * the downloaded binary `gitea-1.12-linux-amd64` is kept at `$HOME`,
 although also copied and installed as `/usr/local/bin/gitea`.

 * `$HOME/gitea-repositories` is a link for the actual repository
 store, `/var/lib/gitea/gitea-repositories`. 

 * `$HOME/postgresql/.s.PGSQL.5432` is a link for the actual
  `postgresql` socket in use, since that moves around a bit if there
  are multiple postgresql version installed (which there are).

 * `$HOME/log` is a link for the gitea log directory,
  `/var/lib/gitea/log`.

 * `$HOME/run.sh` is a by-minute cron bot to ensure gitea is running,
  using `/var/lib/gitea/log/run.log` as "mutex" and for capturing
  stdout/stderr.

 * `$HOME/trimlog.sh` is a daily cron bot to trim down `log/run.log`
  daily, and keep the cut-out with a date tag.

 * `$HOME/rdb.sh` is a helper script for running `psql` on the gitea
  database, if ever it needs some hardcore hands-on.

 * `$HOME/src/gitea` is a clone of
 https://github.com/go-gitea/gitea.git used as the basis for the
 Devuan customisation
 
 * `$HOME/src/setup` is a clone of
   https://gitea.devuan.dev/devuan/gitea.git for the Devuan setup and
   customisation of gitea
 
 * `$HOME/src/trimfile` is a clone of
   https://gitea.devuan.dev/rrq/trimfile.git for the log file trimming
   utility.

`/var/lib/gitea` is a separately mounted file system dedicated for
`gitea`. Everything `gitea` is there, apart from that in `~git/`, and
`/etc/gitea/app.ini`, and the `crontab` for `~git`.
