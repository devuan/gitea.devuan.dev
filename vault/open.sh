#!/bin/bash
#
# Open this vault by mounting it with gocryptfs onto ../temp
#

ROOT=$(git rev-parse --show-toplevel)
[ -z "$ROOT" ] && exit -1

SRC=$ROOT/vault
DST=$ROOT/temp

# The vault is closed ... open it, asking for password
mkdir -p $DST || exit 1
exec gocryptfs -nonempty -q -o allow_other $SRC $DST
